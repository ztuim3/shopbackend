import User from "../models/user.js";
import jwt from "jsonwebtoken";

export default {
  async login(req, res, next) {
    // generate token
    const token = jwt.sign({ id: req.user._id }, process.env.JWT_SECRET, {
      expiresIn: 1200,
    });
    // return
    return res.send({
      token: token,
      privilege: req.user.privilege,
      email: req.body.email,
    });
  },

  async register(req, res, next) {
    const { first_name, last_name, email, password } = req.body;

    if (!first_name || !last_name || !email || !password) next();

    const privilege = "User";
    const user = new User({ first_name, last_name, email, privilege });
    await User.register(user, password);

    res
      .status(200)
      .send({ message: `User created successfully. Now you can log in.` });
  },
};
