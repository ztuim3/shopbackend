import { Router } from "express";
import { catchAsync } from "../middlewares/errors.js";
import productsController from "../controllers/productsController.js";
import jwtAuth from "../middlewares/auth.js";

export default () => {
  const api = Router();

  // GET - One Product - api/products/:ID
  //api.get("/:id", catchAsync(productsController.findOne));

  // GET - All products - api/products/
  api.get("/", catchAsync(productsController.findAll));

  // GET - All products available to buy - api/products/inStore
  api.get("/inStore/", catchAsync(productsController.findInStore));

  // GET All products available in your Cart - api/products/inCart
  api.get("/inCart", jwtAuth, catchAsync(productsController.findYourInCart));

  // GET - All your ordered products - api/products/ordered
  api.get("/ordered", jwtAuth, catchAsync(productsController.findYourOrdered));

  // POST Add product to store - api/products
  api.post("/", jwtAuth, catchAsync(productsController.create));

  //   // PUT api/products/:slug
  //   api.put("/:id", catchAsync(productsController.update));

  // PUT - Move product to client cart - api/products/updateInCart/:ID
  api.put(
    "/updateInCart/:id",
    jwtAuth,
    catchAsync(productsController.updateInCart)
  );

  // PUT - Remove product from client cart - api/products/updateBackToStore/:ID
  api.put(
    "/updateBackToStore/:id",
    jwtAuth,
    catchAsync(productsController.updateBackToStore)
  );

  // PUT - Submit order - api/products/updateSubmitOrder/:email
  api.put(
    "/updateSubmitOrder/:email",
    jwtAuth,
    catchAsync(productsController.updateOrdered)
  );

  // DELETE - Remove product from store - api/products/:ID
  api.delete("/:id", catchAsync(productsController.remove));

  return api;
};
