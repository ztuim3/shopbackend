import mongoose from "mongoose";

const Product = mongoose.Schema(
  {
    title: String,
    brand: String,
    hardDiskSize: String,
    cpuModel: String,
    ramMemory: String,
    graphicsProcessor: String,
    createdBy: String,
    status: String,
    orderedBy: String,
    city: String,
    street: String,
    number: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model("Product", Product);
