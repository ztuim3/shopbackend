import Product from "../models/products.js";
import statusCode from "../config/status.js";

export default {
  //
  async findOne(req, res, next) {
    const product = await Product.findById(req.params.id);
    if (!product) return next(err);
    return res.status(200).send(product);
  },

  async findAll(req, res) {
    const products = await Product.find({}).sort({
      createdAt: "desc",
    });
    return res.status(200).send(products);
  },
  //

  async findInStore(req, res) {
    const products = await Product.find({ status: statusCode.InStore }).sort({
      createdAt: "desc",
    });
    return res.status(200).send(products);
  },

  async findYourInCart(req, res) {
    const products = await Product.find({
      status: statusCode.InCart,
      orderedBy: req.user.email,
    }).sort({
      createdAt: "desc",
    });
    return res.status(200).send(products);
  },

  async findYourOrdered(req, res) {
    const products = await Product.find({
      status: statusCode.Ordered,
      orderedBy: req.user.email,
    }).sort({
      createdAt: "desc",
    });
    return res.status(200).send(products);
  },

  async create(req, res, next) {
    const {
      title,
      brand,
      hardDiskSize,
      cpuModel,
      ramMemory,
      graphicsProcessor,
    } = req.body;

    if (
      !title ||
      !brand ||
      !hardDiskSize ||
      !cpuModel ||
      !ramMemory ||
      !graphicsProcessor
    )
      return next();

    if (req.user.privilege === "Admin") {
      const item = await new Product({
        title: title,
        brand: brand,
        hardDiskSize: hardDiskSize,
        cpuModel: cpuModel,
        ramMemory: ramMemory,
        graphicsProcessor: graphicsProcessor,
        createdBy: req.user.email,
        status: statusCode.InStore,
      }).save();
      return res.status(201).send({ data: item, message: `Item has created` });
    } else next();
  },

  //////

  async update(req, res, next) {
    const item = await Product.findById(req.params.id);

    if (!item) return next(err);

    item.title = req.body.title;
    await item.save();

    return res.status(200).send({ data: item, message: `Item has updated` });
  },

  //////

  async updateInCart(req, res, next) {
    const item = await Product.findById(req.params.id);

    if (!item) return next(err);

    item.orderedBy = req.user.email;
    item.status = statusCode.InCart;

    await item.save();

    return res.status(200).send({ data: item, message: `Item has updated` });
  },

  async updateBackToStore(req, res, next) {
    const item = await Product.findById(req.params.id);

    if (!item) return next(err);

    item.orderedBy = "";
    item.status = statusCode.InStore;

    await item.save();

    return res.status(200).send({ data: item, message: `Item has updated` });
  },

  async updateOrdered(req, res, next) {
    const items = await Product.find({ orderedBy: req.params.email });

    if (!items) return next(err);

    const { city, street, number } = req.body;

    if (!city || !street || !number) return next(err);

    items.forEach(async (item) => {
      item.status = statusCode.Ordered;
      item.city = city;
      item.street = street;
      item.number = number;

      await item.save();
    });

    return res.status(200).send({ message: `Items has updated` });
  },

  async remove(req, res, next) {
    const item = await Product.findById(req.params.id);

    if (!item) return next(err);
    await item.remove();

    return res.status(200).send({ message: `Item has removed` });
  },
};
