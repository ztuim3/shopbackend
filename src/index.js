import dotenv from "dotenv";
dotenv.config({ path: "../.env" });

import express from "express";
import { notFound, catchErrors } from "./middlewares/errors.js";
import products from "./routes/products.js";
import auth from "./routes/auth.js";
import bodyParser from "body-parser";
import passport from "./config/passport.js";
import mongoose from "mongoose";

passport();

mongoose.connect(process.env.MONGO_URL);
mongoose.Promise = global.Promise;
mongoose.connection.on("error", (err) => {
  console.log("Could not connect to the database. Exiting now...");
  process.exit();
});

const app = express();

//app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// routes config
app.use("/api/products", products());
app.use("/api/auth", auth());

// errors handling
app.use(notFound);
app.use(catchErrors);

// let's play!
app.listen(process.env.PORT, () => {
  console.log(`Server is up!`);
});
