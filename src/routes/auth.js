import { Router } from "express";
import { catchAsync } from "../middlewares/errors.js";
import AuthController from "../controllers/authController.js";
import passport from "passport";

export default () => {
  const api = Router();

  // /api/auth/login
  api.post(
    "/login",
    passport.authenticate("local", { session: false }),
    AuthController.login
  );

  // /api/auth/register
  api.post("/register", catchAsync(AuthController.register));

  return api;
};
